// 允许模块下所有类反射
open module blog.web {
//    opens com.hou.blogweb.conf;   允许此包被反射
//    opens com.hou.blogweb.controller to spring.core;   允许此包被spring.core反射
    exports com.hou.blogweb;
    exports com.hou.blogweb.controller;
    exports com.hou.blogweb.conf;
    requires spring.boot.autoconfigure;
    requires org.mybatis.spring;
    requires spring.boot;
    requires mybatis.plus.core;
    requires mybatis.plus.generator;
    requires spring.context;
    requires mybatis.plus.extension;
    requires dynamic.datasource.spring.boot.starter;
    requires spring.web;
    requires java.sql;
    requires log.pojo;
    requires lombok;
    requires spring.webmvc;
    requires spring.core;
    requires spring.beans;
    requires com.google.common;
    requires org.slf4j;
}