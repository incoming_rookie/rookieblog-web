package com.hou.blogweb.controller;

import com.hou.blogpojo.common.JsonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @GetMapping("/test")
    public JsonResult test1(){
        return new JsonResult();
    }
}
