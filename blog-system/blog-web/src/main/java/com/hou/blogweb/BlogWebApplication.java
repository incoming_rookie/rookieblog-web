package com.hou.blogweb;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@MapperScan(basePackages = "com.hou.blogdao.dao") //指定mybatis接口扫描类
@SpringBootApplication
public class BlogWebApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext application=SpringApplication.run(BlogWebApplication.class, args);
        Environment env = application.getEnvironment();
        try {
            String host = InetAddress.getLocalHost().getHostAddress();
            String port=env.getProperty("server.port");
            log.info("""
                    Application '{}' is running! Access URLs:
                    ----------------------------------------------------------
                    Local: http://localhost:{}
                    External: http://{}:{}
                    Doc: http://{}:{}/doc.html
                    SwaggerDoc: http://{}:{}/swagger-ui.html
                    ----------------------------------------------------------
                    """,
            env.getProperty("spring.application.name"),
                    env.getProperty("server.port"),
                    host,port,
                    host,port,
                    host,port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
