module blog.admin {
    requires de.codecentric.spring.boot.admin.server;
    requires spring.boot.autoconfigure;
    requires spring.boot;
}