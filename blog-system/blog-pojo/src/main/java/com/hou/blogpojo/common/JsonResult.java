package com.hou.blogpojo.common;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 接口返回数据封装类
 */
@Data
@Builder  //提供builder创建对象模式
public class JsonResult implements Serializable {
    private static final long serialVersionUID = -3644950655568598241L;

    /**
     * 返回是否成功的状态, 0表示成功,
     * 1或其他值 表示失败
     */
    private ErrorCode state;
    /**
     * 成功时候,返回的JSON数据
     */
    private Object data;


    public JsonResult() {
        state = ErrorCode.SUCCESS;
        data=null;
    }

    public JsonResult(ErrorCode state, Object data) {
        this.data = data;
        this.state = state;
    }

    public JsonResult(Throwable e){
        final ErrorCode fail = ErrorCode.FAIL;
        fail.setMessage(e.getMessage());
        state = fail;
        data=null;
    }

    public JsonResult(Object data){
        state = ErrorCode.SUCCESS;
        this.data=data;
    }
}
