package com.hou.blogpojo.po;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
}
