package com.hou.blogpojo.common;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口返回数据封装类
 */
@Data
@Builder  //提供builder创建对象模式
public class ResponseMessage {
    //请求返回状态码
    private Integer code;
    private String message;
    private Map<String, Object> data;

    public ResponseMessage pushData(String key, Object obj){
        if(this.data == null){
            data = new HashMap<>();
        }
        data.put(key, obj);
        return this;
    }

    public ResponseMessage(Integer code, String message, Map<String, Object> data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResponseMessage pushData(Object object){
        if(null == data){
            data = new HashMap<>();
        }
        if(object == null){
            return this;
        }
        var name = object.getClass().getSimpleName();
        data.put(name.substring(0, 1).toLowerCase() + name.substring(1), object);
        return this;
    }
    public static ResponseMessage success(){
        return new ResponseMessage(ErrorCode.SUCCESS.getCode(), ErrorCode.SUCCESS.getMessage());
    }

    public static ResponseMessage success(ErrorCode systemHttpInfo){
        return new ResponseMessage(systemHttpInfo.getCode(),systemHttpInfo.getMessage());
    }

    public static ResponseMessage success(String messgae){
        return new ResponseMessage(ErrorCode.SUCCESS.getCode(), messgae);
    }

    public static ResponseMessage success(String key, Object obj){
        return new ResponseMessage(ErrorCode.SUCCESS.getCode(), ErrorCode.SUCCESS.getMessage()).pushData(key, obj);
    }

    public static ResponseMessage failture(ErrorCode systemHttpInfo){
        return new ResponseMessage(systemHttpInfo.getCode(),systemHttpInfo.getMessage());
    }
    public static ResponseMessage failture(){
        return new ResponseMessage(ErrorCode.FAIL.getCode(),ErrorCode.FAIL.getMessage());
    }
    public static ResponseMessage newErrorsMessage(String message){
        return new ResponseMessage(ErrorCode.FAIL.getCode(), message);
    }

    public ResponseMessage(@NonNull Integer code, @NonNull String message) {
        this.code = code;
        this.message = message;
    }
}
